import math
a = []
k = 0

def coluna(m):
    # Given the matrix m, return False if the sum of its columns isn't even
    a = []
    for j in range(len(m[0])):
        a.append([m[i][j] for i in range(len(m))])
    for l in range((len(m)) - 1):
        if sum(a[l]) != sum(a[l + 1]):
            return False


def linha(m):
    # Given the matrix m, return False if the sum of the lines isn't even
    for i in range((len(m)) - 1):
        if sum(m[i]) != sum(m[i + 1]):
            return False


def diagonal1(m):
    #Returns the first diagonal of the matrix m
    a = []
    a.append([m[i][i] for i in range(len(m))])
    return a


def diagonal2(m, l):
    #Returns the second diagonal of the matrix m
    global a
    global k
    if l <= 0:
        return a
    a.append(m[k][l - 1])
    l = l - 1
    k += 1
    return diagonal2(m, l)


def magico(m):
    #Returns True if the matrix m is a magic cube and False if it's not
    if linha(m) == False:
        return False
    if coluna(m) == False:
        return False
    f = diagonal1(m)
    l = diagonal2(m, len(m[0]))
    if sum(f[0]) != sum(m[0]):
        return False
    if sum(l) != sum(m[0]):
        return False
    return True

